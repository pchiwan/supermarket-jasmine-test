
angular.module('app.models', [])
	.factory('Producto', function () {
		
		return function (nombre, precio, stock) {
			this.stock = stock ? stock : 0;
			this.nombre = nombre;
			this.precio = precio;
	
			Object.defineProperty(this, 'hayStock', {
				get: function () {
					return this.stock > 0;
				}
			});
			
			this.sumarStock = function () {
				this.stock++;
			};
			
			this.restarStock = function () {
				this.stock--;
			};	
		};
	})
	.factory('CarroCompra', function () {
		
		return function () {
			this.productos = [];
		
			Object.defineProperty(this, 'precioTotal', { 
				get: function () {
					var total = 0;
					for (var i = 0, l = this.productos.length; i < l; i++) {
						total += this.productos[i].precio;
					}
					return total;
				}
			});
			
			this.addProducto = function (nombre, precio) {
				this.productos.push({
					nombre: nombre,
					precio: precio
				});
			};
		};
	})
	.factory('Supermercado', function () {
		
		return function () {
			this.productos = [];
			
			this.addProducto = function (producto) {
				this.productos.push(producto);	
			};
			
			this.solicitarReposicion = function () {
				var faltaStock = [];
				for (var i = 0, l = this.productos.length; i < l; i++) {
					if (this.productos[i].stock() === 0) {
						faltaStock.push({
							nombre: this.productos[i].nombre,
							precio: this.productos[i].precio	
						});		
					}
				}
				return faltaStock;
			};
		};
	});