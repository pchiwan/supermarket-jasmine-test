/// <reference path="../typings/angularjs/angular.d.ts" />

angular.module('app', ['app.services', 'app.models'])
	.controller('superCtrl', ['$scope', 'ServicioReposicion', 'Supermercado', 'CarroCompra', 'Producto', 
	
		function ($scope, ServicioReposicion, Supermercado, CarroCompra, Producto) {
			
			var self = this,
				pale = [];
			
			this.supermercado = new Supermercado();
			this.carrocompra = new CarroCompra();
			
			pale = ServicioReposicion.get();
			
			for (var i = 0, l = pale.length; i < l; i++) {
				var item = pale[i];
				this.supermercado.addProducto(new Producto(item.nombre, item.precio, item.stock));
			}
			
			this.ponerEnCarro = function (producto) {
				if (producto.hayStock) {
					producto.restarStock();
					self.carrocompra.addProducto(producto.nombre, producto.precio);
				}
			};
		}]);
	