angular.module('app.services', [])
	.service('ServicioReposicion', function () {
		
		var productos = [
			{ nombre: 'Donuts', precio: 1.50, stock: 4 },
			{ nombre: 'Natillas', precio: 1.00, stock: 5 },
			{ nombre: 'Huevos', precio: 2.30, stock: 10 },
			{ nombre: 'Leche', precio: 1.85, stock: 3 },
			{ nombre: 'Cerveza 1L', precio: 0.90, stock: 8 },
			{ nombre: 'Zumo naranja', precio: 2.10, stock: 20 },
			{ nombre: 'Pizza bacon', precio: 1.70, stock: 6 },
			{ nombre: 'Galletas chocolate', precio: 3.25, stock: 2 }
		];
		
		this.get = function () {
			return productos;
		};
	});